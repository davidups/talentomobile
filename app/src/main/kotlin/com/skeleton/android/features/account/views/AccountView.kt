package com.skeleton.android.features.account.views

import com.skeleton.android.core.extension.empty
import com.skeleton.android.features.account.models.Account
import com.skeleton.android.features.account.models.AccountEntity

data class AccountView(val accountId: Long, val accountBalanceInCents: Int, val accountCurrency: String, val accountName: String,
                       val accountNumber: String, val accountType: String, val alias: String, val isVisible: Boolean, val iban: String,
                       val linkedAccountId: Long, val productName: String, val productType: Int, val savingsTargetReached: Int, val targetAmountInCents: Int) {

    constructor(accountId: Long, accountBalanceInCents: Int, accountCurrency: String, accountName: String,
                accountNumber: String, accountType: String, alias: String, isVisible: Boolean, iban: String)
            : this(accountId, accountBalanceInCents, accountCurrency, accountName, accountNumber, accountType, alias, isVisible, iban,
            0, String.empty(), 0, 0, 0)

    companion object {
        fun isEmpty() = AccountView(0, 0, String.empty(), String.empty(), String.empty(), String.empty(), String.empty(),
                false, String.empty(), 0, String.empty(), 0, 0, 0)
    }

    fun toAccount(): Account = Account(accountId, accountBalanceInCents, accountCurrency, accountName, accountNumber, accountType, alias, isVisible, iban,
            linkedAccountId, productName, productType, savingsTargetReached, targetAmountInCents)

    fun toAccountEntity(): AccountEntity = AccountEntity(accountId, accountBalanceInCents, accountCurrency, accountName, accountNumber, accountType, alias, isVisible, iban,
            linkedAccountId, productName, productType, savingsTargetReached, targetAmountInCents)
}