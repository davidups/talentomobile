package com.skeleton.android.features.account.views

import androidx.lifecycle.MutableLiveData
import com.skeleton.android.core.platform.BaseViewModel
import com.skeleton.android.features.account.models.Account
import com.skeleton.android.features.account.usescases.GetAccounts
import javax.inject.Inject

class AccountsViewModel
@Inject constructor(private val getAccounts: GetAccounts) : BaseViewModel() {

    val accounts: MutableLiveData<MutableList<AccountView>> = MutableLiveData()

    fun acconts(onlyVisible: Boolean) = getAccounts.invoke(GetAccounts.Params(onlyVisible)) { it.either(::handleFailure, ::handleAccounts) }

    fun handleAccounts(mutableList: MutableList<Account>) {

        this.accounts.value = mutableList.map {
            it.toAccountView()
        }.toMutableList()
    }
}