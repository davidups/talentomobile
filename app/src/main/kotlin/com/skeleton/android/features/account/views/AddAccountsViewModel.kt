package com.skeleton.android.features.account.views

import androidx.lifecycle.MutableLiveData
import com.skeleton.android.core.platform.BaseViewModel
import com.skeleton.android.features.account.models.Account
import com.skeleton.android.features.account.usescases.AddAccounts
import javax.inject.Inject

class AddAccountsViewModel
@Inject constructor(private val addAccounts: AddAccounts) : BaseViewModel() {

    val trigger: MutableLiveData<Any> = MutableLiveData()

    fun addAccount(account: MutableList<Account>) = addAccounts.invoke(AddAccounts.Params(account)) { it.either(::handleFailure, ::notify) }

    private fun notify(any: Any) {
        this.trigger.value = any
    }
}