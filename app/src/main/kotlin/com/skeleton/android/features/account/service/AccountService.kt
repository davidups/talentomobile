package com.skeleton.android.features.account.service

import com.skeleton.android.core.dataBase.AppDatabase
import com.skeleton.android.core.platform.ContextHandler
import com.skeleton.android.features.account.models.AccountEntity
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AccountService
@Inject constructor(contextHandler: ContextHandler) : AccountApi {


    private val accountApi by lazy {
        AppDatabase.getAppDataBase(contextHandler.appContext)?.accountEntityDao()
    }

    override fun addAccounts(account: MutableList<AccountEntity>) = accountApi!!.insertAccounts(account)

    override fun accounts(): MutableList<AccountEntity> = accountApi!!.getAccounts()

    override fun visibleAccounts(): MutableList<AccountEntity> = accountApi!!.getVisibleAccounts(true)
}