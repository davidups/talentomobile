package com.skeleton.android.features.account.usescases

import com.skeleton.android.core.exception.Failure
import com.skeleton.android.core.functional.Either
import com.skeleton.android.core.functional.Either.*
import com.skeleton.android.core.platform.ServiceKOs
import com.skeleton.android.features.account.models.Account
import com.skeleton.android.features.account.service.AccountService
import javax.inject.Inject
import kotlin.Exception

interface AccountRepository {

    fun addAccount(account: MutableList<Account>): Either<Failure, Any>
    fun accounts(onlyVisibles: Boolean): Either<Failure, MutableList<Account>>

    class Network
    @Inject constructor(private val service: AccountService) : AccountRepository {

        override fun addAccount(account: MutableList<Account>): Either<Failure, Any> {
            return try {
                Right(service.addAccounts(account.map {
                    it.toAccountEntity()
                }.toMutableList()))
            } catch (e: Exception) {
                Left(Failure.CustomError(ServiceKOs.DATABASE_ACCESS_ERROR, e.message))
            }
        }

        override fun accounts(onlyVisibles: Boolean): Either<Failure, MutableList<Account>> {
            if (onlyVisibles) {
                return try {
                    Right(service.visibleAccounts().map {
                        it.toAccount()
                    }.toMutableList())
                } catch (e: Exception) {
                    Left(Failure.CustomError(ServiceKOs.DATABASE_ACCESS_ERROR, e.message))
                }
            } else {
                return try {
                    Right(service.accounts().map {
                        it.toAccount()
                    }.toMutableList())
                } catch (e: Exception) {
                    Left(Failure.CustomError(ServiceKOs.DATABASE_ACCESS_ERROR, e.message))
                }
            }
        }
    }
}