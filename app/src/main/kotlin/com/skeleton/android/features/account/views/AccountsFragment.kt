package com.skeleton.android.features.account.views

import android.content.AbstractThreadedSyncAdapter
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.skeleton.android.R
import com.skeleton.android.core.exception.Failure
import com.skeleton.android.core.extension.failure
import com.skeleton.android.core.extension.observe
import com.skeleton.android.core.extension.onClick
import com.skeleton.android.core.extension.viewModel
import com.skeleton.android.core.functional.DialogCallback
import com.skeleton.android.core.platform.BaseFragment
import com.skeleton.android.features.account.models.Account
import com.skeleton.android.features.account.views.AddAccountsViewModel
import com.skeleton.android.features.account.views.adapter.AccountAdapter
import kotlinx.android.synthetic.main.fragment_start.*
import javax.inject.Inject

class AccountsFragment : BaseFragment() {

    override fun layoutId() = R.layout.fragment_start

    @Inject
    lateinit var accountAdapter: AccountAdapter

    private lateinit var addAccountsViewModel: AddAccountsViewModel
    private lateinit var accountsViewModel: AccountsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)

        addAccountsViewModel = viewModel(viewModelFactory) {
            observe(trigger, ::onAccountCreated)
            failure(failure, ::handleFailure)
        }

        accountsViewModel = viewModel(viewModelFactory) {
            observe(accounts, ::renderAccounts)
            failure(failure, ::handleFailure)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        addAccounts()
        initView()
        initListeners()
    }

    private fun addAccounts() {

        showProgress()

        var accounts : MutableList<Account> = arrayListOf()

        accounts.add(Account(748757694, 985000,
                "EUR", "Hr P L G N StellingTD", "748757694",
                "PAYMENT", "", true, "NL88INGB0748757732"))

        accounts.add(Account(700000027559, 1000000,
                "EUR", ",", "748757732",
                "PAYMENT", "", false, "NL23INGB0748757694"))

        accounts.add(Account(700000027560, 15000,
                "EUR", "", "H 177-27066",
                "SAVING", "G\\\\UfffdLSAVINGSdiacrits", true, "", 748757694, "Oranje Spaarrekening", 1000, 1, 2000))

        addAccountsViewModel.addAccount(accounts)
    }

    fun initView() {
        rvAccountList.adapter = accountAdapter
        rvAccountList.layoutManager = LinearLayoutManager(activity)
    }

    private fun initListeners() {

        btnShowAll onClick ::showAllAccounts
        btnShowVisible onClick ::showVisibleAccounts
    }

    private fun onAccountCreated(any: Any?) {
        showAllAccounts()
    }

    private fun showAllAccounts() {
        accountsViewModel.acconts(false)
    }

    private fun showVisibleAccounts() {
        accountsViewModel.acconts(true)
    }

    private fun renderAccounts(accountList: MutableList<AccountView>?) {

        hideProgress()
        accountAdapter.collection = accountList.orEmpty()

    }

    private fun handleFailure(failure: Failure?) {
        when (failure) {
            is Failure.CustomError -> renderFailure(failure.errorCode, failure.errorMessage)
        }
    }

    private fun renderFailure(errorCode: Int, errorMessage: String?) {
        hideProgress()
        showError(errorCode, errorMessage, object : DialogCallback {
            override fun onAccept() {
            }

            override fun onDecline() {
                onBackPressed()
            }
        })
    }
}