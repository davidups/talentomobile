package com.skeleton.android.features.account.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.skeleton.android.features.account.views.AccountView

@Entity
data class AccountEntity(
        @PrimaryKey(autoGenerate = false)
        val accountId: Long,

        @ColumnInfo(name = "Account")
        val accountBalanceInCents: Int,
        val accountCurrency: String,
        val accountName: String,
        val accountNumber: String,
        val accountType: String,
        val alias: String,
        val isVisible: Boolean,
        val iban: String,
        val linkedAccountId: Long,
        val productName: String,
        val productType: Int,
        val savingsTargetReached: Int,
        val targetAmountInCents: Int) {

    fun toAccount(): Account = Account(accountId, accountBalanceInCents, accountCurrency, accountName, accountNumber, accountType, alias, isVisible, iban,
            linkedAccountId, productName, productType, savingsTargetReached,targetAmountInCents)

    fun toAccountView(): AccountView = AccountView(accountId, accountBalanceInCents, accountCurrency, accountName, accountNumber, accountType, alias, isVisible, iban,
            linkedAccountId, productName, productType, savingsTargetReached,targetAmountInCents)
}