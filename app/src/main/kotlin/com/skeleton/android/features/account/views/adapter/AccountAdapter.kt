package com.skeleton.android.features.account.views.adapter

import android.annotation.SuppressLint
import android.net.Uri
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.skeleton.android.R
import com.skeleton.android.core.extension.inflate
import com.skeleton.android.features.account.views.AccountView
import kotlinx.android.synthetic.main.adapter_account_item.view.*
import javax.inject.Inject
import kotlin.properties.Delegates

class AccountAdapter
@Inject constructor(): RecyclerView.Adapter<AccountAdapter.ViewHolder>() {

    internal var collection: List<AccountView> by Delegates.observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(parent.inflate(R.layout.adapter_account_item))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(collection[position])
    }

    override fun getItemCount() = collection.size
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(placeView: AccountView) {

            if (!placeView.accountNumber.isNullOrEmpty()) {
                itemView.tvAccountNumber.text = placeView.accountNumber
            } else {
                itemView.tvAccountNumberInfo.visibility = View.GONE
                itemView.tvAccountNumber.visibility = View.GONE

            }

            if (placeView.accountBalanceInCents != null) {
                itemView.tvAccount.text = placeView.accountBalanceInCents.toString() + " " + placeView.accountCurrency
            } else {
                itemView.tvAccountInfo.visibility = View.GONE
                itemView.tvAccount.visibility = View.GONE
            }

            if (!placeView.accountName.isNullOrEmpty()) {
                itemView.tvAccountName.text = placeView.accountName
            } else {
                itemView.tvAccountNameInfo.visibility = View.GONE
                itemView.tvAccountName.visibility = View.GONE
            }

            if (!placeView.accountType.isNullOrEmpty()) {
                itemView.tvAccountType.text = placeView.accountType
            } else {
                itemView.tvAccountTypeInfo.visibility = View.GONE
                itemView.tvAccountType.visibility = View.GONE
            }

            if (!placeView.alias.isNullOrEmpty()) {
                itemView.tvAlias.text = placeView.alias
            } else {
                itemView.tvAliasInfo.visibility = View.GONE
                itemView.tvAlias.visibility = View.GONE
            }

            if (!placeView.iban.isNullOrEmpty()) {
                itemView.tvIban.text = placeView.iban
            } else {
                itemView.tvIbanInfo.visibility = View.GONE
                itemView.tvIban.visibility = View.GONE
            }

            if (!placeView.productName.isNullOrEmpty()) {
                itemView.tvProductName.text = placeView.productName
            } else {
                itemView.tvProductNameInfo.visibility = View.GONE
                itemView.tvProductName.visibility = View.GONE
            }

            if (placeView.productType != null) {
                itemView.tvProductType.text = placeView.productType.toString()
            } else {
                itemView.tvProductTypeInfo.visibility = View.GONE
                itemView.tvProductType.visibility = View.GONE
            }

            if (placeView.savingsTargetReached != null) {
                itemView.tvSavingsTargetReached.text = placeView.savingsTargetReached.toString()
            } else {
                itemView.tvSavingsTargetReachedInfo.visibility = View.GONE
                itemView.tvSavingsTargetReached.visibility = View.GONE
            }

            if (placeView.targetAmountInCents != null) {
                itemView.tvTargetAmountInCents.text = placeView.targetAmountInCents.toString()
            } else {
                itemView.tvTargetAmountInCentsInfo.visibility = View.GONE
                itemView.tvTargetAmountInCents.visibility = View.GONE
            }
        }
    }
}
