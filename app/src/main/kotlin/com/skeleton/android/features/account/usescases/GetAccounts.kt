package com.skeleton.android.features.account.usescases

import com.skeleton.android.core.interactor.UseCase
import com.skeleton.android.features.account.models.Account
import javax.inject.Inject

class GetAccounts
@Inject constructor(private val accountRepository: AccountRepository) : UseCase<MutableList<Account>, GetAccounts.Params>() {

    override suspend fun run(params: Params) = accountRepository.accounts(params.onlyVisibles)

    class Params (val onlyVisibles: Boolean)
}