package com.skeleton.android.features.account.models

import com.skeleton.android.core.extension.empty
import com.skeleton.android.features.account.views.AccountView

data class Account(val accountId: Long, val accountBalanceInCents: Int, val accountCurrency: String, val accountName: String,
                   val accountNumber: String, val accountType: String, val alias: String, val isVisible: Boolean, val iban: String,
                   val linkedAccountId: Long, val productName: String, val productType: Int, val savingsTargetReached: Int, val targetAmountInCents: Int) {


    constructor(accountId: Long, accountBalanceInCents: Int, accountCurrency: String, accountName: String,
                accountNumber: String, accountType: String, alias: String, isVisible: Boolean, iban: String)
            : this (accountId, accountBalanceInCents,accountCurrency, accountName, accountNumber,accountType,alias,isVisible,iban, 0, String.empty(), 0,0, 0)

    companion object {
        fun isEmpty() = Account(0, 0, String.empty(), String.empty(), String.empty(), String.empty(), String.empty(),
                false, String.empty(), 0, String.empty(), 0, 0,0)
    }

    fun toAccountEntity() : AccountEntity = AccountEntity(accountId, accountBalanceInCents, accountCurrency, accountName, accountNumber, accountType, alias, isVisible, iban,
            linkedAccountId, productName,productType,savingsTargetReached,targetAmountInCents)

    fun toAccountView() : AccountView = AccountView(accountId, accountBalanceInCents, accountCurrency, accountName, accountNumber, accountType, alias, isVisible, iban,
            linkedAccountId, productName,productType,savingsTargetReached,targetAmountInCents)
}