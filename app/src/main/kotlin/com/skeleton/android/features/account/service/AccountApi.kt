package com.skeleton.android.features.account.service

import com.skeleton.android.features.account.models.AccountEntity

internal interface AccountApi {
    fun addAccounts(account: MutableList<AccountEntity>) : Any

    fun accounts() : MutableList<AccountEntity>

    fun visibleAccounts() : MutableList<AccountEntity>
}