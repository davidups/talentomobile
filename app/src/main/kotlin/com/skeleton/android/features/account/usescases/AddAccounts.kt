package com.skeleton.android.features.account.usescases

import com.skeleton.android.core.interactor.UseCase
import com.skeleton.android.features.account.models.Account
import javax.inject.Inject

class AddAccounts
@Inject constructor(private val accountRepository: AccountRepository) : UseCase<Any, AddAccounts.Params>() {


    override suspend fun run(params: Params) = accountRepository.addAccount(params.account)


    class Params (val account: MutableList<Account>)
}