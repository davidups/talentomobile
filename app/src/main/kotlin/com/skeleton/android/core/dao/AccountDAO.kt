package com.skeleton.android.core.dao

import androidx.room.*
import com.skeleton.android.features.account.models.AccountEntity

@Dao
interface AccountDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAccount(account: AccountEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAccounts(accounts: MutableList<AccountEntity>)

    @Query("SELECT * FROM AccountEntity")
    fun getAccounts(): MutableList<AccountEntity>

    @Query("SELECT * FROM AccountEntity WHERE isVisible == :isVisible")
    fun getVisibleAccounts(isVisible: Boolean = true): MutableList<AccountEntity>
}