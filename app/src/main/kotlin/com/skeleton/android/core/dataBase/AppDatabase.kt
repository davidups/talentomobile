package com.skeleton.android.core.dataBase

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import android.content.Context
import com.skeleton.android.core.dao.AccountDAO
import com.skeleton.android.features.account.models.AccountEntity

@Database(entities = [AccountEntity::class], version = 1)
abstract class AppDatabase: RoomDatabase(){
    abstract fun accountEntityDao(): AccountDAO

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getAppDataBase(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                            AppDatabase::class.java,
                            "accountDB").build()
                }
            }
            return INSTANCE
        }

        fun destroyDataBase() {
            INSTANCE = null
        }
    }
}
