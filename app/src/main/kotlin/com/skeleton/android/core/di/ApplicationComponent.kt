package com.skeleton.android.core.di

import com.skeleton.android.AndroidApplication
import com.skeleton.android.core.di.viewmodel.ViewModelModule
import com.skeleton.android.core.navigation.SplashActivity
import com.skeleton.android.features.account.views.AccountsFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, ViewModelModule::class])
interface ApplicationComponent {
    fun inject(application: AndroidApplication)
    fun inject(splashActivity: SplashActivity)
    fun inject(accountsFragment: AccountsFragment)
}