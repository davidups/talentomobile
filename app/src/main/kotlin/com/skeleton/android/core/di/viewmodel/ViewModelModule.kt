package com.skeleton.android.core.di.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.skeleton.android.features.account.views.AccountsViewModel
import com.skeleton.android.features.account.views.AddAccountsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    // =============================================================================================

    @Binds
    @IntoMap
    @ViewModelKey(AddAccountsViewModel::class)
    abstract fun bindsAddAccountViewModel(addAccountsViewModel: AddAccountsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AccountsViewModel::class)
    abstract fun bindsAccountViewModel(accountsViewModel: AccountsViewModel): ViewModel
}